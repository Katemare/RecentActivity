# RecentActivity

Adds parser functions for listing recently created and edited articles

See http://www.mediawiki.org/wiki/Extension:RecentActivity for installation and usage details.
